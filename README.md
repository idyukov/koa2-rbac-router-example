# koa2-rbac-router-example

> Minimalistic application demonstrating usage of `koa2-rbac-router`.

### Install

- Clone source
```
git clone https://bitbucket.org/idyukov/koa2-rbac-router-example
cd koa2-rbac-router-example
```

- Install dependencies:
```
npm install
```
or
```
yarn
```

### Run application
```
npm start
```
or
```
yarn start
```

Application should be available at http://localhost:8080
